import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { MainPageComponent } from './main-page/main-page.component';
import { BookClubComponent } from './book-club/book-club.component';
import { RegisterBookComponent } from './register-book/register-book.component';
import { RecordComponent } from './record/record.component';
import { BorrowBookComponent } from './borrow-book/borrow-book.component';
import { ProfileComponent } from './profile/profile.component';
import { AboutComponent } from './about/about.component';




const routes: Routes = [
  { path: 'register', component: RegistrationComponent },
  { path: 'login', component: LoginComponent },
  { path: 'main', component: MainPageComponent },
  { path: 'book-club', component: BookClubComponent },
  { path: 'register-book', component: RegisterBookComponent },
  { path: 'record', component: RecordComponent },
  { path: 'borrow-book', component: BorrowBookComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'about', component: AboutComponent },
  
  // Add other routes as needed
  { path: '', redirectTo: '/login', pathMatch: 'full' }, // Default route
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}


