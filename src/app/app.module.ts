import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // Add this line

import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { MainPageComponent } from './main-page/main-page.component';
import { BookClubComponent } from './book-club/book-club.component';
import { RegisterBookComponent } from './register-book/register-book.component';
import { RecordComponent } from './record/record.component';
import { BorrowBookComponent } from './borrow-book/borrow-book.component';
import { ProfileComponent } from './profile/profile.component';
import { AboutComponent } from './about/about.component';


@NgModule({
  declarations: [AppComponent, RegistrationComponent, NavbarComponent, LoginComponent, MainPageComponent, BookClubComponent, RegisterBookComponent, RecordComponent, BorrowBookComponent, ProfileComponent, AboutComponent],
  imports: [BrowserModule, FormsModule,AppRoutingModule,HttpClientModule,], // Add FormsModule here
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

