import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  user = {
    username: '',
    password: '',
  };

  constructor(private router: Router) {}

  onSubmit() {
      this.router.navigate(['/main']);

    // Navigate to another page after login (if needed)
    // For example, navigate to a dashboard
    // this.router.navigate(['/dashboard']);
  }
}
